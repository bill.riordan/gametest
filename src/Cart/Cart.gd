extends Actor

const SCALE_X = 0.5
const SCALE_Y = 0.5

const Player = preload("../Player/Player.gd")

onready var sprite = $Sprite
onready var plug_location = $Sprite/PlugLocation
onready var slot_bottom = $Sprite/SlotBottom
onready var slot_middle = $Sprite/SlotMiddle
onready var slot_top = $Sprite/SlotTop
onready var interact_detector = $InteractDetector

onready var _is_powered = true
onready var _filled_amount = 0 # max is 3
onready var _box_1 = null
onready var _box_2 = null
onready var _box_3 = null

signal interact

func _ready():
	var player = get_node("../Player")
	player.connect("interact", self, "_on_Player_interact")

func _physics_process(delta):
	var snap_vector = Vector2.DOWN * FLOOR_DETECT_DISTANCE if _direction.y == 0.0 else Vector2.ZERO
	_velocity = move_and_slide_with_snap(
		_velocity, snap_vector, FLOOR_NORMAL, false, 4, 0.9, false
	)
	
	if _box_1 != null:
		_box_1.carry(slot_bottom.global_position)
	if _box_2 != null:
		_box_2.carry(slot_middle.global_position)
	if _box_3 != null:
		_box_3.carry(slot_top.global_position)

	set_sprite_scale()

#
# Sets the sprite scale
#
# returns null
#
func set_sprite_scale():
	if _direction.x != 0:
		sprite.scale.x = SCALE_X if _direction.x > 0 else -SCALE_X

#
# param Node type The body type you want to find
#
# returns null
#
func find_overlapping_body_type(type):
	var bodies = interact_detector.get_overlapping_bodies()
	for body in bodies:
		if body is type:
			return body
	return null

func _on_Player_interact():
	#find player
	var player_body = find_overlapping_body_type(Player)
	if player_body != null:
		process_player_interaction(player_body)

#
# Process the interaction options of this carried object
#
# returns null
#
func process_player_interaction(player):
	if player._carried_body == null: # player is holding nothing
		# see if we can drop a box for the player
		var box = drop()
		if box != null:
			box.pickup(player)
		return
	else: # player is holding something else
		return

func plug_in_female_plug(female_plug):
	female_plug.carry(plug_location.global_position)

func pickup(body): # this only works on Boxes (need another func for)
	var success = false
	if _filled_amount <= 2:
		match _filled_amount:
			0:
				_box_1 = body
				_filled_amount += 1
				success = true
			1:
				_box_2 = body
				_filled_amount += 1
				success = true
			2:
				_box_3 = body
				_filled_amount += 1
				success = true
	return success

func drop(body = null):
	var box_to_drop = null
	if _filled_amount > 0:
		match _filled_amount:
			1:
				box_to_drop = _box_1
				_box_1 = null
				_filled_amount -= 1
			2:
				box_to_drop = _box_2
				_box_2 = null
				_filled_amount -= 1
			3:
				box_to_drop = _box_3
				_box_3 = null
				_filled_amount -= 1
				
	return box_to_drop

func plug_in_box(box):
	pass

func unplug_box():
	pass
