extends Node2D

const Player = preload("../Player/Player.gd") # Relative path

const LIMIT_LEFT = 0
const LIMIT_TOP = 0
const LIMIT_RIGHT = 955
const LIMIT_BOTTOM = 690

func _ready():
	for child in get_children():
		if child is Player:
			var camera = child.get_node("Camera")
