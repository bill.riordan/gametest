class_name Actor
extends KinematicBody2D

# All Scene Actors inherit this class to give physics simulations

onready var gravity = 800

#pause all physics processes
onready var pause = false

const FLOOR_NORMAL = Vector2.UP
const FLOOR_DETECT_DISTANCE = 8.0

var _velocity = Vector2.ZERO

#intensity of motion (between -1 and 1)
var _direction = Vector2.ZERO

# _physics_process is called after the inherited _physics_process function.
func _physics_process(delta):
	if !pause:
		_velocity.y += gravity * delta
