extends Actor

const SCALE_X = 0.5
const SCALE_Y = 0.5

export var _speed = Vector2(150.0, 350.0)

onready var platform_detector = $PlatformDetector
onready var sprite = $Sprite
onready var animation_player = $AnimationPlayer
onready var attack_timer = $AttackAnimation
onready var gun = $Sprite/Gun
onready var interact_detector = $InteractDetector
onready var carry_location = $Sprite/CarryLocation

onready var _carried_body = null

signal interact

func _physics_process(_delta):
	input_processor()
	
	var snap_vector = Vector2.DOWN * FLOOR_DETECT_DISTANCE if _direction.y == 0.0 else Vector2.ZERO
	var is_on_platform = platform_detector.is_colliding()
	_velocity = move_and_slide_with_snap(
		_velocity, snap_vector, FLOOR_NORMAL, not is_on_platform, 4, 0.9, false
	)
	
	if _carried_body != null:
		_carried_body.carry(carry_location.global_position)
	
	# When the character’s direction changes, we want to to scale the Sprite accordingly to flip it.
	set_sprite_scale()
	
	
	var animation = get_new_animation(false)
	sprite.animation = animation

#
# Drops the object
#
# returns null
#
func input_processor():
	calculate_direction()
	calculate_move_velocity()
	if Input.is_action_just_pressed('interact'):
		#try to interact
		if _carried_body == null:
			emit_signal('interact') # attempt to pick up
		else:
			_carried_body.process_player_interaction(self) # either drop or plug


#
# Sets intensity of motion
#
# returns null
#
func calculate_direction():
	_direction = Vector2(
		Input.get_action_strength("move_right") - Input.get_action_strength("move_left"),
		-1 if is_on_floor() and Input.is_action_just_pressed("jump") else 0
	)

#
# Sets the sprite scale
#
# returns null
#
func set_sprite_scale():
	if _direction.x != 0:
		sprite.scale.x = 1 if _direction.x > 0 else -1

#
# Calculates the true velocity
#
# returns null
#
func calculate_move_velocity():
	_velocity.x = _speed.x * _direction.x
	if _direction.y != 0.0:
		_velocity.y = _speed.y * _direction.y

#
# Gets new animation to play
#
# param bool is_shooting Is the Player shooting
#
# returns string
#
func get_new_animation(is_shooting = false):
	var animation_new = ""
	if is_on_floor():
		animation_new = "right" if abs(_velocity.x) > 0.1 else "right"
	else:
		animation_new = "up" if abs(_velocity.y) > 0 else "up"
	if is_shooting:
		animation_new += "_weapon"
	return animation_new

#
# Finds bodies that overlap this
#
# param Node type The body type you want to find
#
# returns null
#
func find_overlapping_body_type(type):
	var bodies = interact_detector.get_overlapping_bodies()
	for body in bodies:
		if body is type:
			return body
	return null

func pickup(body):
	_carried_body = body

func drop():
	_carried_body = null
