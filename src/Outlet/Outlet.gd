extends KinematicBody2D

const Player = preload("../Player/Player.gd")

onready var plug_location = $Sprite/PlugLocation

onready var _is_powered = true

signal interact

func _ready():
	var player = get_node("../Player")
	player.connect("interact", self, "_on_Player_interact")

func _physics_process(delta):
	return

func _on_Player_interact():
	pass

func plug_in_male_plug(male_plug):
	male_plug.carry(plug_location.global_position)
