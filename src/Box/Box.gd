extends Actor

const SCALE_X = 0.5
const SCALE_Y = 0.5

const Player = preload("../Player/Player.gd")
const Cart = preload("../Cart/Cart.gd")

onready var platform_detector = $PlatformDetector
onready var sprite = $Sprite
onready var animation_player = $AnimationPlayer
onready var interact_detector = $InteractDetector

onready var _is_picked = false

func _ready():
	var player = get_node("../Player")
	player.connect("interact", self, "_on_Player_interact")

# Called when the node enters the scene tree for the first time.
func _physics_process(_delta):
	var snap_vector = Vector2.DOWN * FLOOR_DETECT_DISTANCE if _direction.y == 0.0 else Vector2.ZERO
	_velocity = move_and_slide_with_snap(
		_velocity, snap_vector, FLOOR_NORMAL, false, 4, 0.9, false
	)

	set_sprite_scale()

#
# Sets the sprite scale
#
# returns null
#
func set_sprite_scale():
	if _direction.x != 0:
		sprite.scale.x = SCALE_X if _direction.x > 0 else -SCALE_X

#
# Finds bodies that overlap this
#
# param Node type The body type you want to find
#
# returns null
#
func find_overlapping_body_type(type):
	var bodies = interact_detector.get_overlapping_bodies()
	for body in bodies:
		if body is type:
			return body
	return null

#
# Check if the Player intends to interact with this item
#
# returns null
#
func _on_Player_interact():
	if _is_picked:
		return # we need to ignore commands if it's being carried by a cart
	var player_body = find_overlapping_body_type(Player)
	if player_body != null:
		pickup(player_body)

#
# Process the interaction options of this carried object
#
# returns null
#
func process_player_interaction(player):
	if player._carried_body == self: # player is holding this
		# see if we can plug something in
		drop(player)
		var cart_body = find_overlapping_body_type(Cart)
		if cart_body != null:
			pickup(cart_body)
			return
		else:
			return
		return
	else: # player is holding something else
		return

#
# Picks the item up
#
# returns null
#
func pickup(body):
	body.pickup(self)
	pause = true
	_is_picked = true

#
# Drops the item
#
# returns null
#
func drop(body):
	body.drop()
	_direction = Vector2.ZERO
	_velocity = Vector2.ZERO
	pause = false
	_is_picked = false

#
# Overrides the node's location
#
# returns null
#
func carry(carry_location):
	global_position = carry_location
	
func plug_into_cart(cart):
	pickup(cart)
	return null
