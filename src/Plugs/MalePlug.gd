extends Actor

const SCALE_X = 1
const SCALE_Y = 1

const Player = preload("../Player/Player.gd")
const Outlet = preload("../Outlet/Outlet.gd")

onready var sprite = $Sprite
onready var interact_detector = $InteractDetector

onready var _is_picked = false
onready var _wait = 0

signal interact

signal powered()

func _ready():
	var player = get_node("../Player")
	player.connect("interact", self, "_on_Player_interact")


func _physics_process(_delta):
	var snap_vector = Vector2.DOWN * FLOOR_DETECT_DISTANCE if _direction.y == 0.0 else Vector2.ZERO
	_velocity = move_and_slide_with_snap(
		_velocity, snap_vector, FLOOR_NORMAL, false, 4, 0.9, false
	)

	set_sprite_scale()

#
# Sets the sprite scale
#
# returns null
#
func set_sprite_scale():
	if _direction.x != 0:
		sprite.scale.x = SCALE_X if _direction.x > 0 else -SCALE_X

#
# Finds bodies that overlap this
#
# param Node type The body type you want to find
#
# returns null
#
func find_overlapping_body_type(type):
	var bodies = interact_detector.get_overlapping_bodies()
	for body in bodies:
		if body is type:
			return body
	return null

#
# Check if the Player intends to interact with this item
#
# returns null
#
func _on_Player_interact():
	var player_body = find_overlapping_body_type(Player)
	if player_body != null:
		process_player_interaction(player_body)

#
# Process the various interaction permutations
#
# returns null
#
func process_player_interaction(player):
	if player._carried_body == null: # player is holding nothing
		pickup(player)
		return
	elif player._carried_body == self: # player is holding this
		# see if we can plug something in first
		drop(player)
		var outlet_body = find_overlapping_body_type(Outlet)
		if outlet_body != null:
			player._carried_body = null
			plug_into_outlet(outlet_body)
			return
		else:
			return
		return
	else: # player is holding something else
		return

#
# Picks the item up
#
# returns null
#
func pickup(player):
	player._carried_body = self
	set_animation('out')
	pause = true
	_is_picked = true

#
# Drops the item
#
# returns null
#
func drop(player):
	player._carried_body = null
	_direction = Vector2.ZERO
	_velocity = Vector2.ZERO
	pause = false
	_is_picked = false

#
# Overrides the node's location
#
# returns null
#
func carry(carry_location):
	global_position = carry_location
	
func plug_into_outlet(outlet):
	pause = true
	outlet.plug_in_male_plug(self)
	set_animation('in')
	return null

func set_animation(animation_name):
	sprite.animation = animation_name
